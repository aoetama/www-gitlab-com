---
layout: handbook-page-toc
title: "CSM/CSE Webinar Calendar"
---
# On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

View the [CSM Handbook homepage](/handbook/customer-success/csm/) for additional CSM/CSE-related handbook pages.

Watch previously recorded webinars on our [YouTube Playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0Kpczt4pRtyF147Uvn2bGGvq).

---

# Upcoming Webinars

We’d like to invite you to our free upcoming webinars and workshops in the months of October and November.

If you know someone who might be interested in attending, feel free to share the registration links with them. Everyone is welcome, and we hope to see you there!

## October 2023

#### Hands-on Workshop: Security and Compliance in GitLab
##### October 25th, 2023 at 9:00-11:00AM Pacific Time / 12:00-2:00PM Eastern Time

In this workshop we will focus on how you can secure your application with GitLab. 

We will first take a look at how to apply scanners to your CICD pipelines in a hands-on exercise so that any vulnerabilities are caught as soon as the code is committed. 

Next, we will look at compliance frameworks and pipelines to show how you can ensure no one within your development teams is cutting corners and exposing your application.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_8jTIZyFdTDCh3jvAWckLpg#/registration)

#### DevSecOps/Compliance
##### October 31st, 2023 at 9:00-10:00AM Pacific Time / 12:00-1:00PM Eastern Time

GitLab enables developers and security to work together in a single tool, allowing for proactive security or “shifting left”. This session will cover what GitLab offers, how scan results integrate seamlessly with merge requests, and how to use the Security Dashboard to manage vulnerabilities.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_hOI6GSkQSAK-feMJ4YVxww#/registration)

### EMEA Time Zone Webinars

#### Hands-on Workshop: Security and Compliance in GitLab
##### October 25th, 2023 at 9:00-11:00AM UTC / 11:00AM-1:00PM CET

In this workshop we will focus on how you can secure your application with GitLab. 

We will first take a look at how to apply scanners to your CICD pipelines in a hands-on exercise so that any vulnerabilities are caught as soon as the code is committed. 

Next, we will look at compliance frameworks and pipelines to show how you can ensure no one within your development teams is cutting corners and exposing your application.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_o9tQ9bYkTLWAM7zvxqZ87A#/registration)

#### AI in DevSecOps - GitLab Hands-On Workshop
##### October 25th, 2023 at 12:00-1:30PM UTC / 2:00-3:30PM CET

Join us for a hands-on GitLab AI workshop where we will explore Artificial Intelligence and how it fits within the DevSecOps lifecycle. In this session, we will cover foundational implementation and use case scenarios such as Code Suggestions and Vulnerability Explanations.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_CD5jUPd_SdOe8vPOLOwQWA)

#### DevSecOps/Compliance
##### October 31st, 2023 at 9:00-10AM UTC / 11:00AM-12:00PM CET

GitLab enables developers and security to work together in a single tool, allowing for proactive security or “shifting left”. This session will cover what GitLab offers, how scan results integrate seamlessly with merge requests, and how to use the Security Dashboard to manage vulnerabilities.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_h48TyGjBROGwkt9tDIoZuA#/registration)

## November 2023

### AMER Time Zone Webinars & Workshops

#### Git Basics
##### November 1st, 2023 at 9:00-10:00AM Pacific Time / 12:00-1:00PM Eastern Time

New to Git? Join this webinar targeted at beginners working with source code, where we will review the basics of using Git for version control and how it works with GitLab to help you get started quickly.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_NnX60rz1QRqw2R87gFuTgA#/registration)

#### Intro to GitLab
##### November 7th, 2023 at 9:00-10:00AM Pacific Time / 1:00-2:00PM Eastern Time

Are you new to GitLab? Join this webinar, where we will review what GitLab is, how it benefits you, and the recommended workflow to allow you to get the most out of the platform.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_utYAgNVMTxG7fhD4ymQQKg#/registration)

#### Intro to CI/CD
##### November 14th, 2023 at 9:00-10:00AM Pacific Time / 12:00-1:00PM Eastern Time

Come learn about what CI/CD is and how it can benefit your team. We will cover an overview of CI/CD and what it looks like in GitLab. We will also cover how to get started with your first CI/CD pipeline in GitLab and the basics of GitLab Runners.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_fMoLP8uzQnmpRQBUliSTJw#/registration)

#### Advanced CI/CD
##### November 17th, 2023 at 9:00-10:00AM Pacific Time / 12:00-1:00PM Eastern Time

Expand your CI/CD knowledge while we cover advanced topics that will accelerate your efficiency using GitLab, such as pipelines, variables, rules, artifacts, and more. This session is intended for those who have used CI/CD in the past.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_LsTCwYMSSqOprA0hqJZrBQ#/registration)

#### Security and Compliance
##### November 21st, 2023 at 9:00-10:00AM Pacific Time / 12:00-1:00PM Eastern Time

GitLab enables developers and security to work together in a single tool, allowing for proactive security or “shifting left”. This session will cover what GitLab offers, how scan results integrate seamlessly with merge requests, and how to use the Security Dashboard to manage vulnerabilities.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_xidq5sMsRxS3hMmIt_k5tQ#/registration)

#### Getting Started with DevOps Metrics
##### November 28th, 2023 at 9:00-10:00AM Pacific Time / 12:00-1:00PM Eastern Time

Come learn about DevOps metrics in GitLab and why it is useful to track them. We will cover an overview of DORA metrics, Value Stream Analytics, and Insight dashboards, and what it looks like in Gitlab.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_B9bclrGFQ7ilqiIxJ0lc8A#/registration)

### EMEA Time Zone Webinars and Workshops

#### Git Basics
##### November 1st, 2023 at 9:00-10:00AM UTC / 10:00-11:00AM CET

New to Git? Join this webinar targeted at beginners working with source code, where we will review the basics of using Git for version control and how it works with GitLab to help you get started quickly.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_BbEhaKJ6Sl-Y6u3mKtVBmQ#/registration)

#### Intro to GitLab
##### November 7th, 2023 at 9:00-10:00AM UTC / 10:00-11:00AM CET

Are you new to GitLab? Join this webinar, where we will review what GitLab is, how it benefits you, and the recommended workflow to allow you to get the most out of the platform.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_ywyXdkYrRNm2MDmcySlA5w#/registration)

#### Intro to CI/CD
##### November 14th, 2023 at 9:00-10:00AM UTC / 10:00-11:00AM CET

Come learn about what CI/CD is and how it can benefit your team. We will cover an overview of CI/CD and what it looks like in GitLab. We will also cover how to get started with your first CI/CD pipeline in GitLab and the basics of GitLab Runners.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_DiwBUqBwRTSpJqRycBmcxA#/registration)

#### Advanced CI/CD
##### November 21st, 2023 at 9:00-10:00AM UTC / 10:00-11:00AM CET

Expand your CI/CD knowledge while we cover advanced topics that will accelerate your efficiency using GitLab, such as pipelines, variables, rules, artifacts, and more. This session is intended for those who have used CI/CD in the past.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_yisBO6vWQNO_6A45RlS7Lg#/registration)

#### Security and Compliance
##### November 28th, 2023 at 9:00-10:00AM UTC / 10:00-11:00AM CET

GitLab enables developers and security to work together in a single tool, allowing for proactive security or “shifting left”. This session will cover what GitLab offers, how scan results integrate seamlessly with merge requests, and how to use the Security Dashboard to manage vulnerabilities.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_FywMG2KZQoKIpdvq0D7eIw#/registration)

#### Getting Started with DevOps Metrics
##### November 30th, 2023 at 9:00-10:00AM UTC / 10:00-11:00AM CET

Come learn about DevOps metrics in GitLab and why it is useful to track them. We will cover an overview of DORA metrics, Value Stream Analytics, and Insight dashboards, and what it looks like in Gitlab.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_6UVCDzoZReSZJ4z-pUVscQ#/registration)

### APAC Time Zone Webinars

#### Intro to GitLab
##### November 8th, 2023 at 10:30AM-11:30AM India / 12:00-1:00PM Indonesia / 1:00-2:00PM Singapore / 4:00-5:00PM Sydney 

Are you new to GitLab? Join this webinar, where we will review what GitLab is, how it benefits you, and the recommended workflow to allow you to get the most out of the platform.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_wy4GkKj3SwKivq_ocHPxWA#/registration)

#### Intro to CI/CD
##### November 14th, 2023 at 10:30AM-11:30AM India / 12:00-1:00PM Indonesia / 1:00-2:00PM Singapore / 4:00-5:00PM Sydney

Come learn about what CI/CD is and how it can benefit your team. We will cover an overview of CI/CD and what it looks like in GitLab. We will also cover how to get started with your first CI/CD pipeline in GitLab and the basics of GitLab Runners.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_dAlzlM3BSyqtH7xs6kfJYQ#/registration)

#### Advanced CI/CD
##### November 16th, 2023 at 10:30AM-11:30AM India / 12:00-1:00PM Indonesia / 1:00-2:00PM Singapore / 4:00-5:00PM Sydney

Expand your CI/CD knowledge while we cover advanced topics that will accelerate your efficiency using GitLab, such as pipelines, variables, rules, artifacts, and more. This session is intended for those who have used CI/CD in the past.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_k_P3HkhSTGmBGqxub0D-DQ#/registration)

#### Security and Compliance
##### November 21st, 2023 at 10:30AM-11:30AM India / 12:00-1:00PM Indonesia / 1:00-2:00PM Singapore / 4:00-5:00PM Sydney

GitLab enables developers and security to work together in a single tool, allowing for proactive security or “shifting left”. This session will cover what GitLab offers, how scan results integrate seamlessly with merge requests, and how to use the Security Dashboard to manage vulnerabilities.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_3KwvMBoJQlmkxltx3bXBXA#/registration)


Check back later for more webinars! 
